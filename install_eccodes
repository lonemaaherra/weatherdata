#! /bin/bash

source="$(dirname $(readlink -f $0))/eccodes-2.17.0-Source.tar.gz"
source_name="eccodes-2.17.0-Source"
source_dir=ecc_source
build_dir=ecc_build
assume_yes=0
inst_target=/usr/local
dep=('cmake' 'gcc' 'g++' 'build-essential' 'gfortran' 'python3-pip')
m_dep=()

abort() {
  echo "Aborting ecCodes install."
  exit 1
}

header_print() {
  echo -e "----------------------------------------\n $1\n----------------------------------------"
}

setup_build() {
  header_print "Creating clean build area."
  echo "Creating build source directory."
  mkdir $source_dir
  echo "Creating build directory."
  mkdir $build_dir
  echo "Extracting tarball."
  tar -xzf $source -C $source_dir
}

rm_build() {
  header_print "Cleaning up. Removing build area."
  echo "Deleting build directory."
  rm -rf $build_dir
  echo "Deleting build source directory."
  rm -rf $source_dir
}

check_dep() {
  header_print "Checking dependences."
  m_dep=()
  for i in "${dep[@]}"; do
    echo "-[ $i ]-"
    if ! dpkg-query -s $i | grep "ok installed"; then
      echo "Status: not installed"
      m_dep+=($i)
    fi
  done
}

disp_m_dep() {
  header_print "Missing dependences:"
  for i in "${m_dep[@]}"; do
    echo "  * $i"
  done
}

inst_dep() {
  if [ $assume_yes -eq 1 ]; then
    for i in "${m_dep[@]}"; do
      header_print "Installing package: $i"
      sudo apt-get -y install $i
    done
  else
    echo "Install missing packages [y/N]?"
    read inst
    if [[ $inst == "y" || $inst == "Y" ]]; then
      for i in "${m_dep[@]}"; do
        header_print "Installing package: $i"
        sudo apt-get install $i
      done
    else
      return 1
    fi
  fi
}

inst_eccodes() {
  header_print "Installing ecCodes."
  setup_build
  header_print "Generating build system."
  (cd $build_dir; cmake -DCMAKE_INSTALL_PREFIX=$inst_target -DENABLE_PYTHON=OFF "../$source_dir/$source_name")
  if [ $? -gt 0 ]; then
    rm_build
  else
    header_print "Building ecCodes."
    (cd $build_dir; make)
  fi
  if [ $? -gt 0 ]; then
    rm_build
  else
    header_print "Run test(s) on build."
    (cd $build_dir; ctest)
  fi
  if [ $? -gt 0 ]; then
    rm_build
  else
    header_print "Installing ecCodes to target directory $inst_target."
    (cd $build_dir; make install)
  fi
  rm_build
}

inst_py_eccodes() {
  if [ $assume_yes -eq 0 ]; then
    echo "Install python ecCodes interface [y/N]?"
    read inst
    if ! [[ $inst == "y" || $inst == "Y" ]]; then
      echo "Skipping python ecCodes interface installation."
      return
    fi
  fi
  header_print "Installing python ecCodes interface."
  echo "Installing python eccodes"
  sudo -H pip3 install eccodes
  echo "Setting environment variable for ECCODES_DIR."
  touch /etc/profile.d/eccodes_home.sh
  echo "export ECCODES_DIR=$inst_target" >> /etc/profile.d/eccodes_home.sh
  #export ECCODES_DIR=$inst_target
}

# MAIN
if [ $EUID -ne 0 ]; then
  echo "E: Not root. Please try running $0 as root."
  exit 2
fi

while [ "$1" != "" ]; do
  case $1 in
    -s | --source )
      shift
      source=$1
      source_name=$(basename "${source%.tar.gz}")
      ;;
    -y | --yes | --assume-yes )
      assume_yes=1
      ;;
    -t | --install-target )
      shift
      inst_target=$1
      ;;
  esac
  shift
done

if [[ ! -e $source ]]; then
  echo "Source tarball $source could not be found."
  abort
fi

check_dep
if [ ${#m_dep[@]} -gt 0 ]; then
  disp_m_dep
  inst_dep
  if [ $? -gt 0 ]; then
    echo "E: Dependences not fulfilled."
    abort
  fi
fi
header_print "All dependences fullfilled."

inst_eccodes
inst_py_eccodes

exit 0

